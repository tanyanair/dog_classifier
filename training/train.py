__author__ = 'tnair'
import os
import json
import argparse
from models import cnn, vgg16_pretrained
from data.data_gen import StanfordDogs
from keras import callbacks

# from source.viz.visualizer import plot_images

_MODEL_TYPES = {"cnn": cnn,
                "vgg16_pretrained": vgg16_pretrained}


def _get_cfg():
    parser = argparse.ArgumentParser(description="Main handler for training",
                                     usage="./training/train.py -j training_config.json")

    parser.add_argument("-j", "--json", help="configuration json file", required=True)
    args = parser.parse_args()
    with open(args.json, 'r') as f:
        cfg = json.loads(f.read())

    return cfg


def train(cfg):
    expt_cfg = cfg['experiment']
    expt_name = expt_cfg['name']
    outdir = expt_cfg['outdir']
    os.makedirs(os.path.join(outdir, expt_name), exist_ok=True)

    model_cfg = cfg['model']
    model_type = _MODEL_TYPES[model_cfg['type']]

    model, optimizer = model_type.get_model(model_cfg)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)

    model.summary()

    dataset = StanfordDogs(data_dir=expt_cfg['data_dir'],
                           hdf5file=expt_cfg['hdf5file'],
                           nb_train=expt_cfg['nb_train'],
                           nb_valid=expt_cfg['nb_valid'],
                           nb_class=model_cfg['nb_classes'],
                           input_shape=model_cfg['input_shape'])

    train_gen = dataset.batch_generator(batch_size=expt_cfg['batch_size'], mode='train')
    valid_gen = dataset.batch_generator(batch_size=expt_cfg['batch_size'], mode='valid')

    model_checkpoint = callbacks.ModelCheckpoint(
        os.path.join(outdir, expt_name, 'weights_{epoch:03d}_{val_loss:.3f}.hdf5'),
        monitor='val_loss', save_best_only=False, save_weights_only=True)

    early_stopping = callbacks.EarlyStopping(monitor='val_loss', patience=15, verbose=0, mode='auto')

    model.fit_generator(generator=train_gen,
                        steps_per_epoch=expt_cfg['nb_train'] // expt_cfg['batch_size'],
                        epochs=expt_cfg['nb_epochs'],
                        verbose=1,
                        callbacks=[model_checkpoint, early_stopping],
                        validation_data=valid_gen,
                        validation_steps=expt_cfg['nb_valid'] // expt_cfg['batch_size'],
                        max_q_size=5)


if __name__ == "__main__":
    train(_get_cfg())
