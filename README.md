# Dog Breed Classification

Implementation of two CNN's in Keras for classifying dog breeds from the [Stanford Dogs] Dataset into 1 of 120 classes.

### Usage
 `$ ./train.sh -j ./configs/train_cnn.json`
 
 `$ ./eval.sh -j ./configs/evaluate_cnn.json`

The Python path Theano flags are set in the bash script.

Two json configuration files have been provided that train the two models (VGG16 pretrained, or CNN trained from scratch).
The following items must be modified before running:

- experiment data_dir - points to the local Stanford Dogs dataset directory
- experiment output - points to a directory where model weights will be stored

### Data Preparation 
On the first run, the data will be preprocessed and stored in a .hdf5 file locally at the dataset directory. Subsequent executions will use the .hdf5 file pointed to in the config file.
The data is preprocessed in two different ways (for experimentation purposes).

1. Grayscale images reshaped to (256, 256). This is a standard size, and we can see the impact of colour in breed classification.
2. RGB images (224, 224, 3). The same input size for the VGG16 pretrained network.

In both methods, the images are then normalized by dividing by 255 (the maximum image value). Putting values on [0,1] helps makes the model easier to train because we avoid saturating our activations with huge inputs at the start. 
Keeping the images in an hdf5 is the cleanest, most compressed way to reuse this preprocessing (and it works great with the data generator!)

Before training, the data is shuffled with a set seed and split into 17000 training, 2000 validation, and 1580 test instances. This corresponds to train/valid/test split of approximately (82/10/8)%. Round numbers were chosen for simplicity.

### Model Architectures

#### CNN

![Scheme](readme_assets/cnn_model.png)

Three convolutional blocks each containing:

- Convolutional layer. The number of convolutional kernels is doubled at each stage (rule of thumb, intuitively allows for more complex feature representations as the network grows
- Batch Normalization. Helps the model train by normalizing the inputs to the activations
- Relu activation. Standard activation, makes it easier for gradients to backpropagate (linear outside of the endpoints)
- Max Pooling. Helps regularize by downsampling
- Dropout. Regularizes the network

Starting with a small, simple model like this gives some context to how hard the problem is, and how hard it will be to train.

#### Pretrained VGG16

This network is exactly the same as [Oxford's VGG-16] implementation except for the last three layers (the dense layers), which have been replaced by three smaller dense layers of size (512, 512, 120) respectively instead of (4096, 4096, 1000). The number of neurons in the first two dense layers is reduced to maintain approximately the same ratio with the classification layer. Weights from this model are loaded from the VGG's caffe implementation using the [keras Applications] method.

### Results

Training curves and confusion matrices are given below for the two models. The results are not that good, it's clear the model is training and is doing better than random guessing (~0.8%)


![Scheme](readme_assets/cnn_training.png)

![Scheme](readme_assets/vgg16_training.png)


### CNN Confusion Matrix Test Set

![Scheme](readme_assets/cnn_cmtx.png)

### VGG-16 Confusion Matrix Test Set

![Scheme](readme_assets/vgg16_cmtx.png)


### Overall Accuracy:

![Scheme](readme_assets/test_loss.png)


### Comments
- The learning rate needs to be set very low to avoid the loss from blowing up (ie. 1e-7)
- The VGG-16 model is still training, but the CNN model plateaus after 10 epochs. This is likely because the model is not expressive enough.
- I think the VGG-16 model would perform much better given more time to train. 

### Requirements
Environment: python 3.4

Packages: 

You can install the requirements using the provided requirements.txt file

`pip install -r requirements.txt`


   [Stanford Dogs]: <http://vision.stanford.edu/aditya86/ImageNetDogs/>
   [Oxford's VGG-16]: <https://arxiv.org/abs/1409.1556/>
   [keras Applications]: <https://keras.io/applications/#vgg16/>