from keras.layers import Input, Dropout, Dense, BatchNormalization, Activation, Conv2D, Flatten, MaxPooling2D
from keras.models import Model
from keras.optimizers import Adam


def get_model(config):
    input_shape = tuple(config["input_shape"])
    nb_kers = config['nb_kers']
    dropout = config['dropout']
    lr = config['lr']

    model = _cnn(input_shape, nb_kers, dropout)
    optimizer = Adam(lr=lr)

    return model, optimizer


# Basic network for now, make vgg16 later
def _cnn(input_shape, nb_kers, dr):
    inputs = Input(shape=input_shape, name='input')

    c1 = Conv2D(nb_kers, (3, 3), padding='same', strides=(2, 2))(inputs)
    c1 = BatchNormalization(axis=1)(c1)
    a1 = Activation('relu')(c1)
    p1 = MaxPooling2D(pool_size=(2, 2))(a1)
    p1 = Dropout(dr)(p1)

    c2 = Conv2D(nb_kers * 2, (3, 3), padding='same', strides=(2, 2))(p1)
    c2 = BatchNormalization(axis=1)(c2)
    a2 = Activation('relu')(c2)
    p2 = MaxPooling2D(pool_size=(2, 2))(a2)
    p2 = Dropout(dr)(p2)

    c3 = Conv2D(nb_kers * 4, (3, 3), padding='same', strides=(2, 2))(p2)
    c3 = BatchNormalization(axis=1)(c3)
    a3 = Activation('relu')(c3)
    p3 = MaxPooling2D(pool_size=(2, 2))(a3)
    p3 = Dropout(dr)(p3)

    d4 = Flatten()(p3)
    d4 = Dense(256, activation='relu')(d4)
    a4 = Dropout(dr)(d4)

    d5 = Dense(120, activation='softmax')(a4)

    return Model(inputs, d5)
