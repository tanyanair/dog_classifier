from keras.layers import Input, Dropout, Dense, Flatten
from keras.models import Model
from keras.optimizers import Adam
from keras.applications.vgg16 import VGG16


def get_model(config):
    input_shape = tuple(config["input_shape"])
    dropout = config['dropout']
    lr = config['lr']
    nb_classes = config['nb_classes']

    model = _vgg16(input_shape, nb_classes, dropout)
    optimizer = Adam(lr=lr)

    return model, optimizer


def _vgg16(input_shape, nb_classes, dr):
    inputs = Input(shape=input_shape, name='inputs')

    vgg16_pretrained_model = VGG16(include_top=False, weights='imagenet')

    vgg16_pretrained_out = vgg16_pretrained_model(inputs)

    d1 = Flatten()(vgg16_pretrained_out)
    d1 = Dense(512, activation='relu')(d1)
    d1 = Dropout(dr)(d1)

    d2 = Dense(512, activation='relu')(d1)
    d3 = Dense(nb_classes, activation='softmax')(d2)

    return Model(inputs, d3)
