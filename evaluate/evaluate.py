import os
import json
import argparse
from models import cnn, vgg16_pretrained
from data.data_gen import StanfordDogs
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import numpy as np

_MODEL_TYPES = {"cnn": cnn,
                "vgg16_pretrained": vgg16_pretrained}


def _get_cfg():
    parser = argparse.ArgumentParser(description="Main handler for evaluation",
                                     usage="python ./evaluate/evaluate.py -j evaluate_config.json")

    parser.add_argument("-j", "--json", help="configuration json file", required=True)
    args = parser.parse_args()
    with open(args.json, 'r') as f:
        cfg = json.loads(f.read())

    return cfg


def _to_integers(x_one_hot):
    x_int = np.asarray([np.argmax(row) for row in x_one_hot])
    x_int = np.reshape(x_int, (-1, 1))
    return x_int


"""
Modified from
"http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py"
"""


def plot_cmtx(y_test, y_pred):
    cmtx = confusion_matrix(y_test, y_pred)
    cmtx = cmtx.astype('float') / cmtx.sum(axis=1)[:, np.newaxis]

    plt.imshow(cmtx, interpolation='nearest', cmap='Greys_r')
    plt.title('Normalized Confusion Matrix Test')
    plt.colorbar()

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


def evaluate(cfg):
    expt_cfg = cfg['experiment']
    expt_name = expt_cfg['name']
    outdir = expt_cfg['outdir']
    outdir = os.path.join(outdir, expt_name, 'results')
    os.makedirs(outdir, exist_ok=True)

    model_cfg = cfg['model']
    model_type = _MODEL_TYPES[model_cfg['type']]

    model, optimizer = model_type.get_model(model_cfg)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    dataset = StanfordDogs(data_dir=expt_cfg['data_dir'],
                           hdf5file=expt_cfg['hdf5file'],
                           nb_train=expt_cfg['nb_train'],
                           nb_valid=expt_cfg['nb_valid'],
                           nb_class=model_cfg['nb_classes'],
                           input_shape=model_cfg['input_shape'])

    # train_gen = dataset.batch_generator(batch_size=expt_cfg['batch_size'], mode='test')
    # valid_gen = dataset.batch_generator(batch_size=expt_cfg['batch_size'], mode='valid')
    test_gen = dataset.batch_generator(batch_size=expt_cfg['batch_size'], mode='test')

    model.load_weights(expt_cfg['weights'])

    loss = {}
    # loss['train'] = model.evaluate_generator(train_gen, steps=len(dataset.data_idx['train'])//expt_cfg['batch_size'])
    # loss['valid'] = model.evaluate_generator(valid_gen, steps=len(dataset.data_idx['valid'])//expt_cfg['batch_size'])
    loss['test'] = model.evaluate_generator(test_gen, steps=len(dataset.data_idx['test'])//expt_cfg['batch_size'])

    # print("Train Loss {:.6f}     Train Accuracy = {:.4f}%".format(loss['train'][0], 100 * loss['train'][1]))
    # print("Valid Loss {:.6f}     Valid Accuracy = {:.4f}%".format(loss['valid'][0], 100 * loss['valid'][1]))
    print("Test  Loss {:.6f}     Test  Accuracy = {:.4f}%".format(loss['test'][0], 100 * loss['test'][1]))

    print("Loading all test data")
    test_gen = dataset.batch_generator(batch_size=len(dataset.data_idx['test']), mode='test')
    x_test, y_test = next(test_gen)
    print("Predicting ...")
    y_pred = model.predict(x_test, batch_size=32)

    # convert them back to integer encodings
    y_test = _to_integers(y_test)
    y_pred = _to_integers(y_pred)
    print("Plotting confusion matrix")
    plot_cmtx(y_test, y_pred)


    # which dog are we always predicting? ID number 82
    print(y_pred.max(), y_pred.mean())
    print(dataset.dog_nb_name[int(y_pred.mean())])


if __name__ == "__main__":
    evaluate(_get_cfg())



