#!/usr/bin/env bash
PYTHONPATH=.
THEANO_FLAGS=floatX=float32,device=gpu0,optimizer=fast_run,lib.cnmem=0.2
THEANO_FLAGS=${THEANO_FLAGS},optimizer_excluding=local_softmax_dnn_grad
PYTHONPATH=${PYTHONPATH} THEANO_FLAGS=${THEANO_FLAGS} python evaluate/evaluate.py "$@"
