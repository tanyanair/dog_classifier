h5py==2.7.0
Keras==2.0.4
matplotlib==2.0.1
numpy==1.12.1
Pillow==4.1.1
scipy==0.19.0
Theano==0.9.0
sklearn==0.0
