import os
import numpy as np
from keras.utils.np_utils import to_categorical
from data.preprocess import preprocess
import h5py

np.random.seed(42)


class StanfordDogs:
    def __init__(self, data_dir, nb_train, nb_valid, input_shape, nb_class, hdf5file=None):
        self.data_dir = data_dir
        self.nb_train = nb_train
        self.nb_valid = nb_valid
        self.hdf5file = hdf5file
        self.input_shape = input_shape
        self.nb_class = nb_class
        self.nb_images = 20580
        self.data_idx = {}

        # make the hdf5 if we haven't already
        if not hdf5file:
            preprocess(data_dir, hdf5file, input_shape)

        # make a random list of indices to use for train/test
        data_idx = np.random.permutation(np.arange(self.nb_images))
        self.data_idx['train'] = data_idx[:nb_train]
        self.data_idx['valid'] = data_idx[nb_train:nb_train + nb_valid]
        self.data_idx['test'] = data_idx[nb_train + nb_valid:]
        self.data_idx['all'] = data_idx

        self.dog_name_nb, self.dog_nb_name = self._make_dog_dict()

    def _make_dog_dict(self):
        names = []
        ids = []
        with h5py.File(os.path.join(self.data_dir, self.hdf5file), 'r') as f:
            for i in list(f.keys()):
                names.append(f[i].attrs['breed_name'])
                ids.append(f[i].attrs['breed_nb'])

        names = np.asarray(names)
        ids = np.asarray(ids)

        dog_name_nb = {}
        dog_nb_name = {}
        for i in range(len(names)):
            name = names[i]
            id = ids[i]
            if name not in dog_name_nb.keys():
                dog_name_nb[name] = id
            if id not in dog_nb_name.keys():
                dog_nb_name[id] = name

        return dog_name_nb, dog_nb_name

    def _dog_generator(self, mode='train'):
        data_idx = self.data_idx[mode]

        with h5py.File(os.path.join(self.data_dir, self.hdf5file), 'r') as f:
            while True:
                for i in range(len(data_idx)):
                    img = np.asarray(f[str(i)]['image'], dtype=np.float32)
                    label = to_categorical(f[str(i)].attrs['breed_nb'], self.nb_class)

                    # expand last dimension for the grayscale images
                    if self.input_shape[2] == 1:
                        img = np.expand_dims(img, axis=-1)

                    yield img, label

    def batch_generator(self, batch_size, mode='train'):
        dog_gen = self._dog_generator(mode)

        input_shape = tuple([batch_size, self.input_shape[0], self.input_shape[1], self.input_shape[2]])

        x_concat = np.zeros(shape=input_shape)
        y_concat = np.zeros((batch_size, self.nb_class))

        count = 0

        for x, y in dog_gen:
            if count == batch_size:
                yield x_concat, y_concat
                x_concat = np.zeros(shape=input_shape)
                y_concat = np.zeros((batch_size, self.nb_class))
                count = 0

            x_concat[count] = x
            y_concat[count] = y
            count += 1
