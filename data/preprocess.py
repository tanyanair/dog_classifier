import os
import numpy as np
from scipy.misc import imread, imresize
import h5py

np.random.seed(42)


def preprocess(data_dir, hdf5file, input_shape):

    dog_dirs = os.listdir(data_dir)
    # make sure we only take the folders since there may be .hdf5 files in the path
    dog_dirs = [d for d in dog_dirs if ".hdf5" not in d]

    # get paths to all dogs
    dog_ims = np.asarray([os.path.join(data_dir, dog_dir, dog) for dog_dir in dog_dirs for dog in
                          os.listdir(os.path.join(data_dir, dog_dir))])
    # get labels
    dog_labels = [[d] * len(os.listdir(os.path.join(data_dir, d))) for d in dog_dirs]
    # remove nested labels
    dog_labels = [l for label_list in dog_labels for l in label_list]

    # make dict of labels
    dog_nb_name = {key: label for key, label in enumerate(dog_dirs)}
    dog_name_nb = {label: key for key, label in dog_nb_name.items()}

    # convert the labels to integers
    dog_labels = np.asarray([dog_name_nb[l] for l in dog_labels])

    # shuffle the data
    data = np.concatenate((dog_ims[:, np.newaxis], dog_labels[:, np.newaxis]), axis=1)
    np.random.shuffle(data)

    print('creating hdf5')
    with h5py.File(os.path.join(data_dir, hdf5file), 'w') as f:
        for i in range(len(data)):
            print("{}/{} complete".format(i, len(data)))
            impath = data[i, 0]
            label = int(data[i, 1])

            # greyscale
            if input_shape[-1] == 1:
                img = imresize(imread(impath, flatten=True), tuple(input_shape))
            else:
                img = imresize(imread(impath), tuple(input_shape))
                # one dog has a 4th dimension full of zeros!
                if img.shape[-1] == 4:
                    img = img[:, :, :3]

            img = np.asarray(img, dtype=np.float32)
            img /= 255

            grp = f.create_group(str(i))
            grp.create_dataset('image', data=img, compression='gzip')
            grp.attrs.create('breed_nb', label)
            grp.attrs.create('breed_name', dog_nb_name[label].encode('utf8'))
            grp.attrs.create('breed_nb', label)

# if __name__ == "__main__":
    # preprocess(data_dir='/usr/local/data/tnair/data/stanford_dogs/',
    #            hdf5file='stanford_dogs_224x224x3.hdf5',
    #            input_shape=[224, 224, 3])
    #
    # preprocess(data_dir='/usr/local/data/tnair/data/stanford_dogs/',
    #            hdf5file='stanford_dogs_256x256x1.hdf5',
    #            input_shape=[256, 256, 1])

